# React-webix #

Example of using webix libraby in react application. Do not try this at home.

### How do I get set up? ###

* npm install
* bower install
* npm start


#### TODO List ####

* updating could be improved: 
  for example:
  if only data was updated - table.clearAll() && table.parse(newData)
  if only columns were updated  - table.refreshColumns(newCols, true)
  if only size was updated - table.config.width = newWidth; table.resize();
  other - rerender table
